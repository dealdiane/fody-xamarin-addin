﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace ClassLibrary1
{
    [TransformBindable]
    public class Class2
    {
        public string Something { get; set; }
    }

    [TransformBindable]
    internal class Class1 : BindableObject
    {
        public static readonly BindableProperty StringCommandProperty = BindableProperty.Create("StringCommand", typeof(string), typeof(Class1), default(string));
        public static readonly BindableProperty IsEnabledProperty = BindableProperty.Create("IsEnabled", typeof(Enum1), typeof(Class1), default(Enum1));
        public static readonly BindableProperty NoTransformProperty = BindableProperty.Create("NoTransform", typeof(string), typeof(Class1), null);

        public enum Enum1
        {
            A,
            B,
        }

        [ImplementAutoBindable]
        public DateTime AutoBind { get; set; }

        public string AutoCommand
        {
            get;
            set;
        }

        public string StringCommand
        {
            get;
            set;
        }

        [ImplementAutoBindable]
        public double Double { get; set; }

        [ImplementAutoBindable]
        public float Float { get; set; }

        [ImplementAutoBindable]
        public Guid Guid { get; set; }

        [ImplementAutoBindable]
        public int Int32 { get; set; }

        //[ImplementAutoBindable]
        public Enum1 IsEnabled
        {
            get
            {
                return (Enum1)GetValue(IsEnabledProperty);
            }
            set
            {
                SetValue(IsEnabledProperty, value);
            }
        }

        public string NotAutoBind { get; set; }

        [DoNotTransformBindableProperty]
        public string NoTransform { get; set; }
    }
}