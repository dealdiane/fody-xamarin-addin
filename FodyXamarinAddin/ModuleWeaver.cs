﻿using Mono.Cecil;
using System;
using System.Linq;

public class ModuleWeaver
{
    //private TypeSystem typeSystem;

    // Init logging delegates to make testing easier
    public ModuleWeaver()
    {
        LogInfo = m => { };
    }

    // Will log an informational message to MSBuild
    public Action<string> LogInfo { get; set; }

    // An instance of Mono.Cecil.ModuleDefinition for processing
    public ModuleDefinition ModuleDefinition { get; set; }

    public void Execute()
    {
        LogInfo("Running FodyXamarin");

        try
        {
            new TransformBindable(this).TransformBindables();
        }
        catch(Exception error)
        {
            LogInfo("FodyXamarin ERROR:" + error.ToString());

            throw;
        }
    }
}