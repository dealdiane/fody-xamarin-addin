﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil;

public static class CecilExtensions
{
    public static bool ContainsAttribute(this IEnumerable<CustomAttribute> attributes, string attributeName)
    {
        return attributes.Any(attribute => attribute.Constructor.DeclaringType.Name == attributeName);
    }

    public static List<TypeDefinition> GetAllClasses(this ModuleDefinition moduleDefinition)
    {
        var definitions = new List<TypeDefinition>();
        //First is always module so we will skip that;
        GetTypes(moduleDefinition.Types.Skip(1), definitions);
        return definitions;
    }

    public static IEnumerable<CustomAttribute> GetAttributeByName(this ICustomAttributeProvider type, string attributeName)
    {
        return type.CustomAttributes.Where(a => a.AttributeType.FullName.Equals(attributeName, StringComparison.Ordinal));
    }

    private static void GetTypes(IEnumerable<TypeDefinition> typeDefinitions, List<TypeDefinition> definitions)
    {
        foreach (var typeDefinition in typeDefinitions)
        {
            GetTypes(typeDefinition.NestedTypes, definitions);

            if (typeDefinition.IsInterface)
            {
                continue;
            }
            if (typeDefinition.IsEnum)
            {
                continue;
            }
            definitions.Add(typeDefinition);
        }
    }
}