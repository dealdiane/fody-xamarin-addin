﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mono.Cecil;
using Mono.Cecil.Cil;

public class TransformBindable
{
    private const string _doNotTransformBindablePropertyAttributeName = "DoNotTransformBindablePropertyAttribute";
    private const string _implementAutoBindableAttributeName = "ImplementAutoBindableAttribute";
    private const string _transformBindableAttributeName = "TransformBindableAttribute";
    private static readonly Type[] _primitiveTypes = new[] { typeof(Boolean), typeof(Byte), typeof(Int16), typeof(Int32), typeof(Int64), typeof(Decimal), typeof(SByte), typeof(UInt16), typeof(UInt32), typeof(UInt64) };
    private readonly MethodDefinition _bindableObjectGetValueMethod;
    private readonly MethodDefinition _bindableObjectSetValueMethod;
    private readonly TypeDefinition _bindableObjectType;
    private readonly TypeDefinition _bindablePropertyType;
    private readonly MethodDefinition _createMethod;
    private readonly MethodDefinition _getTypeFromaHandleMethod;
    private readonly TypeDefinition _systemType;

    public TransformBindable(ModuleWeaver weaver)
    {
        if (weaver == null)
        {
            throw new ArgumentNullException("moduleDefinition");
        }

        ModuleDefinition = weaver.ModuleDefinition;
        LogInfo = weaver.LogInfo;

        var xamarinRef = ModuleDefinition.AssemblyReferences.FirstOrDefault(r => r.Name.Equals("Xamarin.Forms.Core", StringComparison.Ordinal));
        var xamarinAsm = ModuleDefinition.AssemblyResolver.Resolve(xamarinRef);

        _bindableObjectType = xamarinAsm.MainModule.GetType("Xamarin.Forms.BindableObject");
        _bindablePropertyType = xamarinAsm.MainModule.GetType("Xamarin.Forms.BindableProperty");
        _createMethod = _bindablePropertyType.Methods.Single(m => m.Name.Equals("Create", StringComparison.Ordinal) && m.Parameters.Count == 10 && m.IsStatic && m.Parameters[0].ParameterType.FullName.Equals(weaver.ModuleDefinition.TypeSystem.String.FullName, StringComparison.Ordinal));
        _bindableObjectGetValueMethod = _bindableObjectType.Methods.Single(m => m.Name.Equals("GetValue", StringComparison.Ordinal));
        _bindableObjectSetValueMethod = _bindableObjectType.Resolve().Methods.Single(m => m.Name.Equals("SetValue", StringComparison.Ordinal) &&
                                                                            m.HasParameters &&
                                                                            m.Parameters.Count == 2 &&
                                                                            m.Parameters[0].ParameterType.FullName.Equals(_bindablePropertyType.FullName, StringComparison.Ordinal) &&
                                                                            m.Parameters[1].ParameterType.FullName.Equals("System.Object", StringComparison.Ordinal));

        _systemType = FindSystemType();

        if (_systemType == null)
        {
            throw new NotSupportedException("Cannot find System.Type");
        }

        _getTypeFromaHandleMethod = _systemType.Methods.Single(m => m.Name.Equals("GetTypeFromHandle", StringComparison.Ordinal));
    }

    public Action<string> LogInfo { get; private set; }

    public ModuleDefinition ModuleDefinition { get; private set; }

    public void TransformBindables()
    {
        foreach (var @class in ModuleDefinition.GetAllClasses()
                                               .Where(c => c.HasCustomAttributes))
        {
            var requiredAttribute = @class.GetAttributeByName(_transformBindableAttributeName).SingleOrDefault();

            if (requiredAttribute == null)
            {
                continue;
            }

            if (!IsBindableType(@class))
            {
                LogInfo(String.Format("\tSkipping {0} because it does not inherit {1}.", @class.FullName, _bindableObjectType.FullName));
                continue;
            }

            foreach (var property in @class.Properties)
            {
                var doNotProcessAttribute = property.GetAttributeByName(_doNotTransformBindablePropertyAttributeName).SingleOrDefault();

                if (doNotProcessAttribute != null)
                {
                    continue;
                }

                // Check whether a bindable property field exists. This is a required field in Xamarin for bindables to work.
                var fieldName = property.Name + "Property";
                var bindableField = @class.Fields.SingleOrDefault(f => f.Name.Equals(fieldName, StringComparison.Ordinal) && f.FieldType.FullName.Equals(_bindablePropertyType.FullName, StringComparison.Ordinal));

                if (bindableField == null)
                {
                    // The required bindable field doesn't exist. Check whether we are told to create it automatically.
                    var autoBindableAttribute = property.GetAttributeByName(_implementAutoBindableAttributeName).SingleOrDefault();

                    // Do not create field and do not process this property
                    if (autoBindableAttribute == null)
                    {
                        continue;
                    }

                    // Create and initialize bindable field
                    bindableField = CreateAndInitializeBindableField(@class, property, fieldName);
                }

                // Replace getter/setter
                if (property.GetMethod != null)
                {
                    ReplaceMethodBody(property.GetMethod, il =>
                    {
                        il.Emit(OpCodes.Ldarg_0);
                        il.Emit(OpCodes.Ldsfld, bindableField);
                        il.Emit(OpCodes.Call, property.GetMethod.Module.Import(_bindableObjectGetValueMethod));

                        var propertyType = property.PropertyType.Resolve();

                        if (propertyType.IsValueType || IsTypeEqual(propertyType, typeof(string)))
                        {
                            il.Emit(OpCodes.Unbox_Any, property.PropertyType);
                        }
                        else
                        {
                            il.Emit(OpCodes.Isinst, property.PropertyType);
                            //il.Emit(OpCodes.Stloc_0);
                            //il.Emit(OpCodes.Ldloc_0);
                        }

                        il.Emit(OpCodes.Ret);
                    });
                }

                if (property.SetMethod != null)
                {
                    ReplaceMethodBody(property.SetMethod, il =>
                    {
                        il.Emit(OpCodes.Ldarg_0);
                        il.Emit(OpCodes.Ldsfld, bindableField);
                        il.Emit(OpCodes.Ldarg_1);

                        if (property.PropertyType.IsValueType)
                        {
                            il.Emit(OpCodes.Box, property.PropertyType);
                        }

                        il.Emit(OpCodes.Call, property.SetMethod.Module.Import(_bindableObjectSetValueMethod));
                        il.Emit(OpCodes.Ret);
                    });
                }

                // Remove backing field for automatic properties
                var backingField = @class.Fields.SingleOrDefault(f => f.Name.Equals("<" + property.Name + ">k__BackingField", StringComparison.Ordinal));

                if (backingField != null)
                {
                    @class.Fields.Remove(backingField);
                }

                LogInfo(String.Format("\tProcessed {1}::{0}.", property.Name, @class.FullName));
            }
        }

        CleanReferences();
    }

    private static void ClearMethodBody(ILProcessor il)
    {
        while (il.Body.Instructions.Count > 0)
        {
            il.Remove(il.Body.Instructions[0]);
        }
    }

    private static void InsertInstructionsAtStartOfMethod(MethodDefinition method, IEnumerable<Instruction> instructions)
    {
        var body = method.Body;
        var firstInstruction = body.Instructions.First();
        var il = body.GetILProcessor();

        foreach (var instruction in instructions)
        {
            il.InsertBefore(firstInstruction, instruction);
        }
    }

    private static bool IsTypeEqual(TypeDefinition definition, Type type)
    {
        return definition.FullName.Equals(type.FullName, StringComparison.Ordinal);
    }

    private static void ReplaceMethodBody(MethodDefinition method, Action<ILProcessor> ilCallback)
    {
        var il = method.Body.GetILProcessor();

        ClearMethodBody(il);

        ilCallback(il);
    }

    private void CleanReferences()
    {
        // Remove all FodyXamarin custom attributes
        foreach (var @class in ModuleDefinition.GetAllClasses()
                                               .Where(c => c.HasCustomAttributes))
        {
            var classAttributes = @class.GetAttributeByName(_transformBindableAttributeName).ToList();

            foreach (var attribute in classAttributes)
            {
                @class.CustomAttributes.Remove(attribute);
            }

            foreach (var property in @class.Properties)
            {
                var propertyAttributes = property.GetAttributeByName(_doNotTransformBindablePropertyAttributeName)
                                                    .Concat(property.GetAttributeByName(_implementAutoBindableAttributeName))
                                                    .ToList();

                foreach (var attribute in propertyAttributes)
                {
                    property.CustomAttributes.Remove(attribute);
                }
            }
        }

        var referenceToRemove = ModuleDefinition.AssemblyReferences.FirstOrDefault(x => x.Name == "FodyXamarin");

        if (referenceToRemove == null)
        {
            LogInfo("\tNo reference to 'FodyXamarin' found. References not modified.");
            return;
        }

        ModuleDefinition.AssemblyReferences.Remove(referenceToRemove);
        LogInfo("\tRemoved reference to 'FodyXamarin'.");
    }

    private FieldDefinition CreateAndInitializeBindableField(TypeDefinition @class, PropertyDefinition property, string fieldName)
    {
        var staticConstructor = GetStaticConstructor(@class);
        var bindableField = new FieldDefinition(fieldName, FieldAttributes.Static | FieldAttributes.InitOnly | FieldAttributes.Public, @class.Module.Import(_bindablePropertyType));

        @class.Fields.Add(bindableField);

        InsertInstructionsAtStartOfMethod(staticConstructor, GetBindableFieldInitializeInstructions(staticConstructor, property, bindableField));

        return bindableField;
    }

    private TypeDefinition FindSystemType()
    {
        var asmResolver = ModuleDefinition.AssemblyResolver;

        foreach (var assembly in new[]
                                 {
                                     asmResolver.Resolve("mscorlib"),
                                     asmResolver.Resolve("System.Runtime"),
                                     asmResolver.Resolve("System")
                                 }
            /*.Concat(ModuleDefinition.AssemblyReferences.Select(a => asmResolver.Resolve(a)))*/)
        {
            if (assembly == null)
            {
                continue;
            }

            var systemType = assembly.MainModule.GetType("System.Type");

            if (systemType != null)
            {
                return systemType;
            }
        }

        return null;
    }

    private static bool IsTypePrimitive(TypeDefinition type)
    {
        return _primitiveTypes.Any(t => IsTypeEqual(type, t));
    }

    private IEnumerable<Instruction> GetBindableFieldInitializeInstructions(MethodDefinition staticConstructor, PropertyDefinition property, FieldDefinition bindableField)
    {
        yield return Instruction.Create(OpCodes.Ldstr, property.Name);
        yield return Instruction.Create(OpCodes.Ldtoken, property.PropertyType);
        yield return Instruction.Create(OpCodes.Call, staticConstructor.Module.Import(_getTypeFromaHandleMethod));
        yield return Instruction.Create(OpCodes.Ldtoken, staticConstructor.DeclaringType);
        yield return Instruction.Create(OpCodes.Call, staticConstructor.Module.Import(_getTypeFromaHandleMethod));

        var propertyType = property.PropertyType.Resolve();

        // Create default value
        if (IsTypePrimitive(propertyType) || IsTypeEqual(propertyType, typeof(Single)) || IsTypeEqual(propertyType, typeof(Double)) || property.PropertyType.IsPrimitive)
        {
            if (IsTypeEqual(propertyType, typeof(Single)))
            {
                yield return Instruction.Create(OpCodes.Ldc_R4, 0f);
            }
            else if (IsTypeEqual(propertyType, typeof(Double)))
            {
                yield return Instruction.Create(OpCodes.Ldc_R8, 0d);
            }
            else
            {
                yield return Instruction.Create(OpCodes.Ldc_I4_0);
            }

            if (IsTypeEqual(propertyType, typeof(Int64)) || IsTypeEqual(propertyType, typeof(UInt64)))
            {
                yield return Instruction.Create(OpCodes.Conv_I8);
            }
            else if (IsTypeEqual(propertyType, typeof(Decimal)))
            {
                yield return Instruction.Create(OpCodes.Newobj, propertyType);
            }

            yield return Instruction.Create(OpCodes.Box, property.PropertyType);
        }
        else if (property.PropertyType.IsValueType)
        {
            var localVar = new VariableDefinition(staticConstructor.Module.Import(property.PropertyType));
            staticConstructor.Body.Variables.Add(localVar);

            yield return Instruction.Create(OpCodes.Ldloca_S, localVar);
            yield return Instruction.Create(OpCodes.Initobj, property.PropertyType);
            yield return Instruction.Create(OpCodes.Ldloc, localVar);
            yield return Instruction.Create(OpCodes.Box, property.PropertyType);

            if (!staticConstructor.Body.InitLocals)
            {
                staticConstructor.Body.InitLocals = true;
            }
        }
        else
        {
            yield return Instruction.Create(OpCodes.Ldnull);
        }

        yield return Instruction.Create(OpCodes.Ldc_I4_2);
        yield return Instruction.Create(OpCodes.Ldnull);
        yield return Instruction.Create(OpCodes.Ldnull);
        yield return Instruction.Create(OpCodes.Ldnull);
        yield return Instruction.Create(OpCodes.Ldnull);
        yield return Instruction.Create(OpCodes.Ldnull);
        yield return Instruction.Create(OpCodes.Call, staticConstructor.Module.Import(_createMethod));
        yield return Instruction.Create(OpCodes.Stsfld, bindableField);
    }

    private MethodDefinition GetStaticConstructor(TypeDefinition @class)
    {
        var staticConstructor = @class.Methods.SingleOrDefault(m => m.Name == ".cctor" && m.IsConstructor && m.IsStatic);

        if (staticConstructor == null)
        {
            staticConstructor = new MethodDefinition(".cctor", MethodAttributes.Private | MethodAttributes.Static | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName, ModuleDefinition.TypeSystem.Void);
            staticConstructor.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));

            @class.Methods.Add(staticConstructor);
        }

        return staticConstructor;
    }

    private bool IsBindableType(TypeDefinition type)
    {
        if (type.FullName.Equals(_bindableObjectType.FullName, StringComparison.Ordinal))
        {
            return true;
        }

        if (type.BaseType == null)
        {
            return false;
        }

        return IsBindableType(type.BaseType.Resolve());
    }
}