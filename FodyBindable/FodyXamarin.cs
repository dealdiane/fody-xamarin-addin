﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
public class TransformBindableAttribute : Attribute
{
}

[AttributeUsage(AttributeTargets.Property, AllowMultiple=false)]
public class DoNotTransformBindablePropertyAttribute : Attribute
{

}

[AttributeUsage(AttributeTargets.Property, AllowMultiple=false)]
public class ImplementAutoBindableAttribute : Attribute
{
}